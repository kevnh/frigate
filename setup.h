// Filename: setup.h
// Author: Kevin Huynh
// Email: k_huynh13@u.pacific.edu

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef SETUP_H
#define SETUP_H

struct Ship {
	char symbol;
	int size, vertical, row, col, hits, destroyed;
};

struct Ship ***board;
struct Ship **ships;
char **display;
int gridSize;
int numShips;
int ammo;
int games;
int wins;

struct Ship*** generateBoard();
void clearBoard();
struct Ship** generateShips();
void setupBoard(char *name);
void setupShip(struct Ship *ship, char symbol);
void getRandPosition(struct Ship *ship);
void placeShip(struct Ship *ship);
int intersect(struct Ship *ship, int row, int col);
char** generateDisplay();
void setupDisplay();
void freeBoard();
void freeShips();
void freeDisplay();

int checkGridSize();
void printDisplay();
void revealShip(struct Ship *ship);
void revealShips();
int play();
int checkGameStatus();
int checkValidInput(char letter);
int playerMove();
int checkMove(char letter, int num);
void handleMove(int row, int col);
int checkShipStatus(struct Ship *ship);
void clearBuffer();

#endif
