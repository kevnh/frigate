// Filename: setup.c
// Author: Kevin Huynh
// Email: k_huynh13@u.pacific.edu

#include "setup.h"

struct Ship*** generateBoard() {
	board = (struct Ship***)malloc(sizeof \
		(struct Ship**) *gridSize);
	for (int i = 0; i < gridSize; i++) {
		board[i] = (struct Ship**)malloc(sizeof \
		(struct Ship*) *gridSize);
		for (int j = 0; j < gridSize; j++) {
			board[i][j] = (struct Ship*)malloc(sizeof \
			(struct Ship) *1);
			board[i][j] = NULL;
		}
	}	
	return board;
}

void clearBoard() {
	for (int i = 0; i < gridSize; i++) {
		for (int j = 0; j < gridSize; j++) {
			board[i][j] = NULL;
		}
	}
}

struct Ship** generateShips() {
	ships = (struct Ship**)malloc(sizeof(struct Ship*) *numShips);
	for (int i = 0; i < numShips; i++) {
		ships[i] = (struct Ship*)malloc(\
		sizeof(struct Ship) *1);
	}
	return ships;
}

void setupBoard(char *name) {
	for (int i = 0; i < numShips; i++) {
		struct Ship *temp = ships[i];
		free(temp);
	}
	
	if (name == NULL) {
		setupShip(ships[0], 'c');
		setupShip(ships[1], 'b');
		setupShip(ships[2], 'f');
		setupShip(ships[3], 'f');
		
		for (int i = 0; i < numShips; i++) {
			getRandPosition(ships[i]);
			placeShip(ships[i]);
		}
	}
	else {
		FILE *fp;
		char symbol, letter, str[60];
		int vertical, row, i = 0;
		fp = fopen(name, "r");
		if (!fp) {
			perror("File opening failed");
			exit(1);
			return;
		}
		while (fgets(str, 60, fp) != NULL) {
			sscanf(str, "%c %i %c%i", &symbol, \
			&vertical, &letter, &row);

			setupShip(ships[i], symbol);
			ships[i]->vertical = vertical;

			ships[i]->col = letter - 'a';
			ships[i]->row = row - 1;
			placeShip(ships[i]);
			i++;
		}
		
		fclose(fp);
	}
}

void setupShip(struct Ship *ship, char symbol) {
	ship->symbol = symbol;
	ship->vertical = rand() % 2;
	ship->hits = 0;
	ship->destroyed = 0;
	switch(symbol) {
		case 'c':
			ship->size = 5;
			break;

		case 'b':
			ship->size = 4;
			break;

		case 'f':
			ship->size = 2;
			break;
		
		default:
			break;
	}
}

void getRandPosition(struct Ship *ship) {
	int row = rand() % gridSize;
	int col = rand() % gridSize;
	while (row + (ship->size * ship->vertical) \
		> gridSize || \
		col + (ship->size * (1 - ship->vertical)) \
		> gridSize || \
		intersect(ship, row, col) == 1)  {
		row = rand() % gridSize;
		col = rand() % gridSize;
		ship->vertical = rand() % 2;
	}
	ship->row = row;
	ship->col = col;
}

void placeShip(struct Ship *ship) {
	for (int i = 0; i < ship->size; i++) {
		board[ship->row+(i*ship->vertical)]\
		[ship->col+(i*(1-ship->vertical))] = ship;
	}
}

int intersect(struct Ship *ship, int row, int col) {
	for (int i = 0; i < ship->size; i++) {
		if (board[row+(i*ship->vertical)]\
			[col+(i*(1-ship->vertical))] != NULL) {
			return 1;
		}
	}
	return 0;
}

char** generateDisplay() {
	display = (char**)malloc(sizeof(char*) *(gridSize + 2));
	for (int i = 0; i < gridSize + 2; i++) {
		display[i] = (char*)malloc(sizeof(char) \
		*(gridSize + 3));
	}
	return display;
}

void setupDisplay() {
	char letter = 'A';
	for (int i = 0; i < 3; i++) {
		display[0][i] = ' ';
		display[1][i] = ' ';
	}
	display[1][2] = '+';	
	for (int i = 0; i < gridSize; i++) {
		display[0][i+3] = letter;
		display[1][i+3] = '-';

		if (((i + 1) / 10) == 0) {
			display[i+2][0] = ' ';
		}
		else {
			display[i+2][0] = ((i + 1) / 10) + '0';
		}
		display[i+2][1] = ((i + 1) % 10) + '0';
		display[i+2][2] = '|';
		letter++;
		for (int j = 0; j < gridSize; j++) {
			display[i+2][j+3] = '.';
		}
	}
}

void freeBoard() {
	for (int i = 0; i < gridSize; i++) {
		for (int j = 0; j < gridSize; j++) {
			if (board[i][j] == NULL) {
				free(board[i][j]);
			}
		}
		free(board[i]);
	}
	free(board);
	board = NULL;
}

void freeShips() {
	for (int i = 0; i < numShips; i++) {
		free(ships[i]);
	}
	free(ships);
	ships = NULL;
}

void freeDisplay() {
	for (int i = 0; i < gridSize + 2; i++) {
		free(display[i]);
	}
	free(display);
	display = NULL;
}
