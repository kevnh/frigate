// Filename: main.c
// Author: Kevin Huynh
// Email: k_huynh13@u.pacific.edu

#include "setup.h"

int main(int argc, char *argv[]) {
	int finished = 0;
	numShips = 4;
	games = 0;
	wins = 0;
	gridSize = 5;
	
	printf("Argument count:%i\n", argc);

	srand(time(NULL));

	printf("%s \n", "How big should the grid size be? (Only the first 3 integers will be used)");
	scanf("%3i", &gridSize);
	clearBuffer();
	gridSize = checkGridSize();
	
	board = generateBoard();
	ships = generateShips();
	display = generateDisplay();

	while (finished == 0) {
		games++;

		setupBoard(argv[1]);

		setupDisplay();
		ammo = (gridSize * gridSize) / 2;

		finished = play();

		clearBoard();
	}
	
	printf("\nYou won %i out of %i games.\n", wins, games);

	freeBoard();
	freeShips();
	freeDisplay();
	return 0;
}

int checkGridSize() {
	if (gridSize < 5) {
		gridSize = 5;
		printf("%s \n", "The minimum grid size is 5, so we'll just do that.");
		return 5;
	}
	else if (gridSize > 20) {
		gridSize = 20;
		printf("%s \n", "The maximum grid size is 20, so we'll just do that.");
		return 20;
	}
	return gridSize;
}	

void printDisplay() {
	for (int i = 0; i < gridSize + 2; i++) {
		for (int j = 0; j < gridSize + 3; j++) {
			printf("%c ", display[i][j]);
		}
		printf("\n");
	}
}

void revealShip(struct Ship *ship) {
	for (int i = 0; i < ship->size; i++) {
		if (display[ship->row+(i*ship->vertical)+2]\
			[ship->col+(i*(1-ship->vertical))+3] != 'h') {
			display[ship->row+(i*ship->vertical)+2]\
			[ship->col+(i*(1-ship->vertical))+3] = \
			ship->symbol;
		}
	}
}

void revealShips() {
	for (int i = 0; i < numShips; i++) {
		revealShip(ships[i]);
	}
}

int play() {
	int finished = 0;
	int move;
	char input = ' ';
	int validInput = 0;
	while (finished == 0) {
		printDisplay();
		move = playerMove();
		handleMove((move % 100), (move / 100));
		ammo--;
	
		finished = checkGameStatus();
	}

	printf("\n%s \n", "Here are all the ship locations.");
	revealShips();
	printDisplay();
	
	printf("%s \n", "Would you like to play again? (y/n)");
	
	while (validInput == 0) {
		scanf("%1c", &input);
		clearBuffer();
		validInput = checkValidInput(input);
	}

	if (input == 'y' || input == 'Y') {
		return 0;
	}
	else {
		return 1;
	}
}

int checkGameStatus() {	
	int destroyedShips = 0;
	for (int i = 0; i < numShips; i++) {
		if (ships[i]->destroyed == 1) {
			destroyedShips++;
		}
	}
	if (ammo == 0 && destroyedShips != numShips) {
		printf("\n%s \n", "You ran out of ammo.");
		return 1;
	}
	else if (destroyedShips == numShips) {
		printf("\n%s \n", "You won!");
		wins++;
		return 1;
	}
	return 0;
}

int checkValidInput(char letter) {
	if (letter == 'y' || letter == 'n' || letter == 'Y' || \
		letter == 'N') {
		return 1;
	}
	else {
		printf("Invalid input, please try again.\n");
		return 0;
	}
}

int playerMove() {
	char letter = ' ';
	int numLet = 0;
	int num = 0;
	int finished = 0;
	printf("%s%i%s \n", "Enter a coordinate for your shot (", \
		ammo, " shots remaining):");

	while (finished == 0) {
		scanf("%1c%3i", &letter, &num);
		clearBuffer();
		finished = checkMove(letter, num);
	}
	
	if (letter >= 'a' && letter <= ('a' + gridSize - 1)) {
		numLet = letter - ('a');
	}
	else {
		numLet = letter - ('A');
	}

	numLet = numLet * 100;
	num = num - 1;
	return numLet + num;
}

int checkMove(char letter, int num) {
	if (((letter >= 'A' && letter <= ('A' + gridSize - 1)) || \
		(letter >= 'a' && letter <= ('a' + gridSize - 1))) && \
		(num > 0 && num <= gridSize)) {
		return 1;
	}
	printf("%s \n", "Invalid input, please try again.");
	return 0;
}

void handleMove(int row, int col) {
	struct Ship *temp = board[row][col];
	if (temp == NULL) {
		display[row+2][col+3] = 'm';
	}
	else {
		display[row+2][col+3] = 'h';
		temp->hits++;
		if (checkShipStatus(temp) == 1) {
			revealShip(temp);
			temp->destroyed = 1;
		}
	}
}

int checkShipStatus(struct Ship *ship) {
	if (((double)(ship->hits) / ship->size) > .7) {
		return 1;
	}
	return 0;
}

void clearBuffer() {
	char c;
	while ((c = getchar()) != '\n' && c != EOF) {

	}
}
